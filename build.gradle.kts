import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

plugins {
    java
    kotlin("jvm") version "1.5.10"
    id("com.github.johnrengelman.shadow") version "6.0.0"
}

group = "nb.util"
version = "1.0-0"

repositories {
    mavenCentral()
}

fun DependencyHandlerScope.localImpl(fileName: String, path: String = "lib/") = implementation(files("$path$fileName"))

fun DependencyHandlerScope.localJar(fileName: String, path: String = "lib/") = localImpl("$fileName.jar", path)


dependencies {
    implementation(kotlin("stdlib"))
    localJar("arguments-1.0-0")
}

tasks.withType<ShadowJar> {
    archiveBaseName.set("requirement")
    archiveVersion.set("1.0-0")
    minimize()
    dependencies {
        exclude(dependency(".*:.*:.*"))
    }
}
