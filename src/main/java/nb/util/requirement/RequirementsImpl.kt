package nb.util.requirement

class RequirementsImpl(
    requirements: List<Requirement>
): Requirements {
    override val requirements = requirements.toHashSet()
}
