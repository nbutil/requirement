package nb.util.requirement

import nb.util.arguments.ArgumentContext
import nb.util.arguments.Arguments

interface Requirement: (Arguments) -> Boolean {

    fun requirement(argContext: ArgumentContext): Boolean

    fun onFail(argContext: ArgumentContext)

    override operator fun invoke(arguments: Arguments): Boolean {
        val context = ArgumentContext(arguments)
        val result = requirement(context)
        if (!result) {
            onFail(context)
        }
        return result
    }

    class Builder(
        @property:RequirementDSL inline var requirementImpl: RequirementSignature = ::RequirementImpl,
        @property:RequirementDSL inline var requirement: ArgumentContext.() -> Boolean = { true },
        @property:RequirementDSL inline var onFail: ArgumentContext.() -> Unit = { }
    ) {
        @RequirementDSL fun requirementImpl(block: RequirementSignature): Requirement.Builder {
            requirementImpl = block
            return this
        }

        @RequirementDSL fun requirement(block: ArgumentContext.() -> Boolean): Requirement.Builder {
            requirement = block
            return this
        }

        @RequirementDSL fun onFail(block: ArgumentContext.() -> Unit): Requirement.Builder {
            onFail = block
            return this
        }

        fun build(): Requirement = requirementImpl(requirement, onFail)
    }
}