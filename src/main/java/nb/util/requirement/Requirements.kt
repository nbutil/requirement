package nb.util.requirement

import nb.util.arguments.ArgumentContext
import nb.util.arguments.Arguments


interface Requirements: (Arguments) -> Boolean, Requirement, Iterable<Requirement> {
    val requirements: Collection<Requirement>

    override fun invoke(arguments: Arguments): Boolean = requirements.all { it(arguments) }

    override fun requirement(argContext: ArgumentContext) = requirements.all { it.requirement(argContext) }

    override fun onFail(argContext: ArgumentContext) = requirements.forEach {
        val result = it.requirement(argContext)
        if (!result) {
            it.onFail(argContext)
        }
    }

    override fun iterator(): Iterator<Requirement> = requirements.iterator()

    class Builder(
        @property:RequirementsDSL var requirementsImpl: RequirementsSignature = ::RequirementsImpl
    ){
        private val requirements: MutableList<Requirement> = mutableListOf()

        @RequirementsDSL fun addRequirement(requirement: Requirement) {
            requirements.add(requirement)
        }

        @RequirementsDSL inline fun addRequirements(vararg requirements: Requirement) {
            requirements.forEach(::addRequirement)
        }

        @RequirementsDSL inline fun require(noinline block: Requirement.Builder.() -> Unit) {
            addRequirement(buildRequirement(block))
        }

        fun build(): Requirements = requirementsImpl(requirements)
    }
}
