package nb.util.requirement

import nb.util.arguments.ArgumentContext

@DslMarker
annotation class RequirementDSL

typealias RequirementSignature = (ArgumentContext.() -> Boolean, ArgumentContext.() -> Unit) -> Requirement

inline fun buildRequirement(block: Requirement.Builder.() -> Unit) = Requirement.Builder().apply(block).build()

@DslMarker
annotation class RequirementsDSL

typealias RequirementsSignature = (List<Requirement>) -> Requirements

inline fun requirementsOf(
    vararg requirements: Requirement,
    requirementsImpl: RequirementsSignature = ::RequirementsImpl
): Requirements = requirementsImpl(requirements.toList())

inline fun buildRequirements(block: Requirements.Builder.() -> Unit) = Requirements.Builder().apply(block).build()

@RequirementsDSL
class RequirementsContext(
    private val _requirements: MutableList<Requirement>
) {

    @RequirementsDSL
    fun addRequirement(requirement: Requirement) {
        _requirements.add(requirement)
    }

    @RequirementsDSL
    inline fun addRequirements(requirement: Requirement, vararg requirements: Requirement) {
        addRequirement(requirement)
        requirements.forEach(::addRequirement)
    }

    @RequirementsDSL
    inline fun addRequirements(requirements: Requirements) {
        requirements.forEach(::addRequirement)
    }

    @RequirementsDSL
    inline fun addRequirement(noinline block: Requirement.Builder.() -> Unit) {
        addRequirement(buildRequirement(block))
    }
}
