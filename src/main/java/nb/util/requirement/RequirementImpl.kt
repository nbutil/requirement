package nb.util.requirement

import nb.util.arguments.ArgumentContext

data class RequirementImpl(
    private inline val requirementFunction: ArgumentContext.() -> Boolean,
    private inline val onFailFunction: ArgumentContext.() -> Unit
): Requirement {
    override fun requirement(argContext: ArgumentContext) = requirementFunction(argContext)
    override fun onFail(argContext: ArgumentContext) = onFailFunction(argContext)
}
